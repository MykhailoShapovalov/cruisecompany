package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Booking;
import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.repository.BookingRepository;
import com.shapovalov.cruise_company.repository.CruiseRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class BookingServiceTest {

    @Mock
    private BookingRepository bookingRepository;

    @Mock
    private CruiseRepository cruiseRepository;

    private BookingService underTest;
    private Booking booking;
    private Cruise cruiseFromDB;
    private Ship shipFromDB;

    @BeforeEach
    void setUp() {
        underTest = new BookingService(bookingRepository, cruiseRepository);

        booking = new Booking(1L, 1L, false, false, false, true);

        Set<Ship> ships = new HashSet<>();
        Set<Cruise> cruises = new HashSet<>();

        shipFromDB = new Ship(1, "shipName", 900,
                0, 24, cruises);

        ships.add(shipFromDB);

        cruiseFromDB = new Cruise(1L, "cruiseName", "route",
                LocalDateTime.now(), LocalDateTime.now(), 5,
                BigDecimal.valueOf(1000), true, 6, ships);

        cruises.add(cruiseFromDB);
    }

    @Test
    void checkIsApplyToCruiseSetsNewFields() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(bookingRepository.save(any())).thenReturn(booking);

        Booking newBooking = underTest.applyToCruise(userId, cruiseId);

        assertEquals(userId, newBooking.getUserId());
        assertEquals(cruiseId, newBooking.getCruiseId());
        assertFalse(newBooking.isApproved());
        assertFalse(newBooking.isDocumentUpload());
        assertFalse(newBooking.isPaidUp());
        assertTrue(newBooking.isActive());
    }

    @Test
    void confirmApplyToCruiseShouldReturnFalseIfCruiseNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(cruiseRepository.findById(any())).thenReturn(Optional.empty());

        assertFalse(underTest.confirmApplyToCruise(userId, cruiseId));
    }

    @Test
    void confirmApplyToCruiseShouldReturnFalseIfNoShipsWithFreePlaces() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(cruiseRepository.findById(any())).thenReturn(Optional.of(cruiseFromDB));

        shipFromDB.setCurrentNumOfPeople(shipFromDB.getCapacity());

        assertFalse(underTest.confirmApplyToCruise(userId, cruiseId));
    }

    @Test
    void confirmApplyToCruiseShouldReturnFalseIfBookingNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(cruiseRepository.findById(any())).thenReturn(Optional.of(cruiseFromDB));
        Mockito.when(bookingRepository.findById(any())).thenReturn(Optional.empty());

        assertFalse(underTest.confirmApplyToCruise(userId, cruiseId));
    }

    @Test
    void confirmApplyToCruiseShouldReturnFalseIfBookingAlreadyApproved() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(cruiseRepository.findById(any())).thenReturn(Optional.of(cruiseFromDB));
        Mockito.when(bookingRepository.findById(any())).thenReturn(Optional.of(booking));

        booking.setApproved(true);

        assertFalse(underTest.confirmApplyToCruise(userId, cruiseId));
    }

    @Test
    void confirmApplyToCruiseShouldReturnTrueAndCheckIsCurrentNumOfPeopleIncreased() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(cruiseRepository.findById(any())).thenReturn(Optional.of(cruiseFromDB));
        Mockito.when(bookingRepository.findById(any())).thenReturn(Optional.of(booking));

        assertTrue(underTest.confirmApplyToCruise(userId, cruiseId));
        assertTrue(booking.isApproved());
    }

    @Test
    void updateIsDocumentUploadShouldReturnFalseIfBookingNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(bookingRepository.findById(any())).thenReturn(Optional.empty());

        assertFalse(underTest.updateIsDocumentUpload(userId, cruiseId));
    }

    @Test
    void updateIsDocumentUploadShouldReturnTrueAndCheckIfDocUploadTrue() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(bookingRepository.findById(any())).thenReturn(Optional.of(booking));

        assertTrue(underTest.updateIsDocumentUpload(userId, cruiseId));
        assertTrue(booking.isDocumentUpload());
    }

    @Test
    void isBookingExistShouldReturnTrueIfBookingExist() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(bookingRepository.findById(any())).thenReturn(Optional.of(booking));

        assertTrue(underTest.isBookingExist(userId, cruiseId));
    }

    @Test
    void isBookingExistShouldReturnTrueIfBookingNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;

        Mockito.when(bookingRepository.findById(any())).thenReturn(Optional.empty());

        assertFalse(underTest.isBookingExist(userId, cruiseId));
    }

    @Test
    void checkIsBookingActiveSetFalseIfStartDateBeforeNow() {
        Long cruiseId = 1L;

        List<Cruise> cruises = new ArrayList<>();
        cruiseFromDB.setStartDate(LocalDateTime.now());
        cruises.add(cruiseFromDB);

        List<Booking> bookings = new ArrayList<>();
        booking.setActive(true);
        bookings.add(booking);

        Mockito.when(cruiseRepository.findAll()).thenReturn(cruises);
        Mockito.when(bookingRepository.findByCruiseId(cruiseId)).thenReturn(bookings);
        Mockito.when(bookingRepository.save(booking)).thenReturn(booking);

        underTest.checkIsBookingActive();

        assertFalse(booking.isActive());
    }

    @Test
    void checkIsBookingActiveDoNotSetFalseIfStartDateAfterNow() {
        List<Cruise> cruises = new ArrayList<>();
        cruiseFromDB.setStartDate(LocalDateTime.MAX);
        cruises.add(cruiseFromDB);

        Mockito.when(cruiseRepository.findAll()).thenReturn(cruises);

        underTest.checkIsBookingActive();

        assertTrue(booking.isActive());
    }
}