package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.repository.CruiseRepository;
import com.shapovalov.cruise_company.repository.ShipRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CruiseServiceTest {

    @Mock
    private CruiseRepository cruiseRepository;

    @Mock
    private ShipRepository shipRepository;

    @Mock
    private ShipService shipService;

    private CruiseService underTest;
    private Cruise cruiseFromDB;
    private Ship shipFromDB;

    @BeforeEach
    void setUp() {
        underTest = new CruiseService(cruiseRepository, shipRepository, shipService);

        Set<Ship> ships = new HashSet<>();
        Set<Cruise> cruises = new HashSet<>();

        shipFromDB = new Ship(1, "shipName", 900,
                0, 24, cruises);

        ships.add(shipFromDB);

        cruiseFromDB = new Cruise(1L, "cruiseName", "route",
                LocalDateTime.now(), LocalDateTime.now(), 5,
                BigDecimal.valueOf(1000), true, 6, ships);

        cruises.add(cruiseFromDB);
    }

    @Test
    void findCruiseByIdIfItExist() {
        Long id = 1L;

        Mockito.when(cruiseRepository.findById(id)).thenReturn(Optional.of(cruiseFromDB));

        assertEquals(cruiseFromDB, underTest.findCruiseById(id));
    }

    @Test
    void findCruiseByIdIfItNotExist() {
        Long id = 1L;

        Mockito.when(cruiseRepository.findById(id)).thenReturn(Optional.empty());

        assertNull(underTest.findCruiseById(id));
    }

    @Test
    void testGetAll() {
        List<Cruise> list = new ArrayList<>();
        list.add(cruiseFromDB);

        Mockito.when(cruiseRepository.findAll()).thenReturn(list);

        assertEquals(cruiseFromDB.getId(), underTest.getAll().stream().findFirst().get().getId());
    }

    @Test
    void testFindPaginatedWithSorting() {
        int pageNumber = 1;
        int pageSize = 1;
        String sortField = "sortField";
        String sort = "sort";

        Page page = mock(Page.class);

        Mockito.when(cruiseRepository.findAll((Pageable) Mockito.any())).thenReturn(page);

        assertEquals(page.getTotalElements(), underTest.findPaginated(pageNumber, pageSize, sortField, sort).getTotalElements());
    }

    @Test
    void checkFreePlacesIfCruiseNotExist() {
        Long id = 1L;

        Mockito.when(cruiseRepository.findById(id)).thenReturn(Optional.empty());

        underTest.checkFreePlaces(id);

        verify(cruiseRepository, times(0)).save(cruiseFromDB);
    }

    @Test
    void checkFreePlacesIfTheyAre() {
        Long id = 1L;

        Mockito.when(cruiseRepository.findById(id)).thenReturn(Optional.of(cruiseFromDB));
        Mockito.when(cruiseRepository.save(cruiseFromDB)).thenReturn(cruiseFromDB);

        cruiseFromDB.setFreePlaces(false);

        underTest.checkFreePlaces(id);

        assertTrue(cruiseFromDB.isFreePlaces());

        verify(cruiseRepository, times(1)).save(cruiseFromDB);
    }

    @Test
    void checkFreePlacesIfTheyNot() {
        Long id = 1L;
        int fullLoad = 900;

        Mockito.when(cruiseRepository.findById(id)).thenReturn(Optional.of(cruiseFromDB));
        Mockito.when(cruiseRepository.save(cruiseFromDB)).thenReturn(cruiseFromDB);

        cruiseFromDB.getShips().stream().findFirst().get().setCurrentNumOfPeople(fullLoad);
        cruiseFromDB.setFreePlaces(true);

        underTest.checkFreePlaces(id);

        assertFalse(cruiseFromDB.isFreePlaces());

        verify(cruiseRepository, times(1)).save(cruiseFromDB);
    }

    @Test
    void shouldReturnFalseIfCruiseFormNullSaveNewCruise() {
        assertFalse(underTest.saveNewCruise(null));
    }

    @Test
    void checkIsSaveNewCruiseSetsFreePlaces() {
        cruiseFromDB.setFreePlaces(false);

        Mockito.when(cruiseRepository.save(cruiseFromDB)).thenReturn(cruiseFromDB);

        assertTrue(underTest.saveNewCruise(cruiseFromDB));
        assertTrue(cruiseFromDB.isFreePlaces());

        verify(cruiseRepository, times(1)).save(cruiseFromDB);
    }

    @Test
    void shouldReturnFalseIfCruiseIdNotExistDeleteById() {
        Long id = 1L;

        Mockito.doThrow(IllegalArgumentException.class).when(cruiseRepository).deleteById(anyLong());

        assertFalse(underTest.deleteById(id));
    }

    @Test
    void shouldReturnTrueIfCruiseIdExistDeleteById() {
        Long id = 1L;

        Mockito.doNothing().when(cruiseRepository).deleteById(anyLong());

        assertTrue(underTest.deleteById(id));
    }

    @Test
    void checkAddShipToCruiseIfCruiseNotExist() {
        Long cruiseId = 1L;
        int shipId = 1;

        Mockito.when(cruiseRepository.findById(cruiseId)).thenReturn(Optional.empty());
        Mockito.when(shipService.findShipById(shipId)).thenReturn(shipFromDB);

        assertFalse(underTest.addShipToCruise(cruiseId, shipId));
    }

    @Test
    void checkAddShipToCruiseIfShipNotExist() {
        Long cruiseId = 1L;
        int shipId = 1;

        Mockito.when(cruiseRepository.findById(cruiseId)).thenReturn(Optional.of(cruiseFromDB));
        Mockito.when(shipService.findShipById(shipId)).thenReturn(null);

        assertFalse(underTest.addShipToCruise(cruiseId, shipId));
    }

    @Test
    void checkAddShipToCruiseIf() {
        Long cruiseId = 1L;
        int shipId = 1;

        cruiseFromDB.setShips(new HashSet<>());
        shipFromDB.setShips_cruises(new HashSet<>());

        Mockito.when(cruiseRepository.findById(cruiseId)).thenReturn(Optional.of(cruiseFromDB));
        Mockito.when(shipService.findShipById(shipId)).thenReturn(shipFromDB);
        Mockito.when(cruiseRepository.save(cruiseFromDB)).thenReturn(cruiseFromDB);
        Mockito.when(shipRepository.save(shipFromDB)).thenReturn(shipFromDB);

        assertTrue(underTest.addShipToCruise(cruiseId, shipId));

        assertEquals(cruiseFromDB.getShips().size(), shipFromDB.getShips_cruises().size());
        assertEquals(shipFromDB.getShipName(), cruiseFromDB.getShips().stream().findFirst().get().getShipName());
    }

    @Test
    void checkIsUpdateCruiseSetsFields() {
        Long id = 1L;
        Cruise cruiseBeforeUpdate = new Cruise();

        Mockito.when(cruiseRepository.getById(id)).thenReturn(cruiseBeforeUpdate);
        Mockito.when(cruiseRepository.save(cruiseBeforeUpdate)).thenReturn(cruiseBeforeUpdate);

        underTest.updateCruise(cruiseFromDB, id);

        assertEquals(cruiseFromDB.getCruiseName(), cruiseBeforeUpdate.getCruiseName());
        assertEquals(cruiseFromDB.getRoute(), cruiseBeforeUpdate.getRoute());
        assertEquals(cruiseFromDB.getStartDate(), cruiseBeforeUpdate.getStartDate());
        assertEquals(cruiseFromDB.getEndDate(), cruiseBeforeUpdate.getEndDate());
        assertEquals(cruiseFromDB.getPortsDuringRoute(), cruiseBeforeUpdate.getPortsDuringRoute());
        assertEquals(cruiseFromDB.getPrice(), cruiseBeforeUpdate.getPrice());
        assertEquals(cruiseFromDB.getDuration(), cruiseBeforeUpdate.getDuration());
        assertEquals(cruiseFromDB.isFreePlaces(), cruiseBeforeUpdate.isFreePlaces());
    }

    @Test
    void checkFindPaginated() {
        int pageNumber = 1;
        int pageSize = 1;

        Page page = mock(Page.class);

        Mockito.when(cruiseRepository.findAll((Pageable) Mockito.any())).thenReturn(page);

        assertEquals(page.getTotalElements(), underTest.findPaginated(pageNumber, pageSize).getTotalElements());
    }
}