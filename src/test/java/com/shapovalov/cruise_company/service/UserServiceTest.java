package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.*;
import com.shapovalov.cruise_company.repository.BookingRepository;
import com.shapovalov.cruise_company.repository.RoleRepository;
import com.shapovalov.cruise_company.repository.UserRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private BookingRepository bookingRepository;
    @Mock
    private CruiseService cruiseService;
    @Mock
    private BookingService bookingService;
    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private UserService underTest;

    private User userFromDB;
    private Cruise cruiseFromDB;

    @BeforeEach
    void setUp() {
        underTest = new UserService(userRepository, roleRepository, bookingRepository, cruiseService, bookingService, bCryptPasswordEncoder);

        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));

        userFromDB = new User(1L, "agentSmith", "123456789a", "agentSmith@gmail.com", "John", "Smith", "0996573123",
                new BigDecimal(100), LocalDateTime.now(), LocalDateTime.now(), "123456789a", roles);

        Set<Ship> ships = new HashSet<>();
        ships.add(new Ship());

        cruiseFromDB = new Cruise(1L, "testCruise", "testRoute", LocalDateTime.now(), LocalDateTime.now(), 4, new BigDecimal(1000), true, 5, ships);
    }

    @Test
    void loadExistedUserByUsername() {
        String username = "agentSmith";

        Mockito.when(userRepository.findByUsername(username)).thenReturn(Optional.of(userFromDB));

        assertEquals(userFromDB.getUsername(), underTest.loadUserByUsername(username).getUsername());
    }

    @Test
    void loadNotExistedUserByUsername() {
        String username = "notExistedUsername";

        Mockito.when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        assertNull(underTest.loadUserByUsername(username));
    }

    @Test
    void checkIsEmailExistIfEmailExist() {
        String email = "agentSmith@gmail.com";

        Mockito.when(userRepository.findByEmail(email)).thenReturn(Optional.of(userFromDB));

        assertTrue(underTest.isEmailExist(email));
    }

    @Test
    void checkIsEmailExistIfEmailNotExist() {
        String email = "notExistedEmail";

        Mockito.when(userRepository.findByEmail(email)).thenReturn(Optional.empty());

        assertFalse(underTest.isEmailExist(email));
    }


    @Test
    void findUserByIdThatNotExist() {
        Long id = 1L;

        Mockito.when(userRepository.findById(id)).thenReturn(Optional.empty());

        assertNull(underTest.findUserById(id));
    }

    @Test
    void findUserByIdThatExist() {
        Long id = 1L;

        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(userFromDB));

        assertEquals(userFromDB, underTest.findUserById(id));
    }

    @Test
    void checkIsUsernameExistIfUsernameExist() {
        String userName = "agentSmith";

        Mockito.when(userRepository.findByUsername(userName)).thenReturn(Optional.of(userFromDB));

        assertTrue(underTest.isUsernameExist(userName));
    }

    @Test
    void checkIsUsernameExistIfUsernameNotExist() {
        String userName = "notExistedUsername";

        Mockito.when(userRepository.findByUsername(userName)).thenReturn(Optional.empty());

        assertFalse(underTest.isUsernameExist(userName));
    }

    @Test
    void checkSaveNewUserIfUserNull() {
        assertFalse(underTest.saveNewUser(null));
    }

    @Test
    void checkIsSaveNewUserSetsFields() {
        Role role = new Role(1L, "ROLE_USER");
        LocalDateTime localDateTimeBeforeSaving = LocalDateTime.now();
        BigDecimal balanceAfterSaving = BigDecimal.ZERO;

        Mockito.when(roleRepository.getById(1L)).thenReturn(new Role(1L, "ROLE_USER"));
        Mockito.when(bCryptPasswordEncoder.encode(userFromDB.getPassword())).thenReturn(userFromDB.getPassword());
        Mockito.when(userRepository.save(userFromDB)).thenReturn(userFromDB);

        assertTrue(underTest.saveNewUser(userFromDB));
        assertEquals(balanceAfterSaving, userFromDB.getAccount());
        assertTrue(userFromDB.getCreated().isAfter(localDateTimeBeforeSaving));
        assertEquals(role.getRoleName(), userFromDB.getRoles().stream().findFirst().get().getRoleName());
    }

    @Test
    void checkSetAdminRole() {
        Role role = new Role(2L, "ROLE_ADMIN");

        Mockito.when(roleRepository.getById(2L)).thenReturn(role);

        underTest.setAdminRole(userFromDB);
        assertTrue(userFromDB.getRoles()
                .stream()
                .findFirst().get()
                .getRoleName()
                .equalsIgnoreCase(
                        role.getRoleName()));
    }

    @Test
    void checkIfUpdateUserSetsFields() {
        Long id = 1L; // id юзера з бд
        User updatedUser = new User(); // юзер поля якому має встановити метод
        LocalDateTime beforeUpdate = LocalDateTime.now();

        Mockito.when(userRepository.getById(id)).thenReturn(updatedUser);
        Mockito.when(userRepository.save(updatedUser)).thenReturn(updatedUser);

        underTest.updateUser(id, userFromDB);

        assertTrue(updatedUser.getFirstName().equalsIgnoreCase(userFromDB.getFirstName()));
        assertTrue(updatedUser.getLastName().equalsIgnoreCase(userFromDB.getLastName()));
        assertTrue(updatedUser.getEmail().equalsIgnoreCase(userFromDB.getEmail()));
        assertTrue(updatedUser.getPhone().equalsIgnoreCase(userFromDB.getPhone()));
        assertTrue(updatedUser.getUpdated().isAfter(beforeUpdate));
    }

    @Test
    void deleteUserIfExist() {
        Long id = 1L;

        doNothing().when(userRepository).deleteById(id);

        assertTrue(underTest.deleteUser(id));

        verify(userRepository, times(1)).deleteById(id);
    }

    @Test
    void deleteUserIfNotExist() {
        Long id = 1L;

        doThrow(IllegalArgumentException.class).when(userRepository).deleteById(id);

        assertFalse(underTest.deleteUser(id));
    }

    @Test
    void checkIsUpdateBalanceWork() {
        Long id = 1L;
        BigDecimal updateBalance = BigDecimal.valueOf(100);
        BigDecimal oldBalance = userFromDB.getAccount();

        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(userFromDB));
        Mockito.when(userRepository.save(userFromDB)).thenReturn(userFromDB);

        assertTrue(underTest.updateBalance(id, updateBalance));

        assertEquals(oldBalance.add(updateBalance), userFromDB.getAccount());
    }

    @Test
    void checkUpdateBalanceIfUserNotExist() {
        Long id = 1L;
        BigDecimal updateBalance = BigDecimal.valueOf(100);

        Mockito.when(userRepository.findById(id)).thenReturn(Optional.empty());

        assertFalse(underTest.updateBalance(id, updateBalance));
    }

    @Test
    void checkPaidForCruiseIfUserNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;
        String expected = "error";

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(cruiseFromDB);

        assertEquals(expected, underTest.paidForCruise(userId, cruiseId));
    }

    @Test
    void checkPaidForCruiseIfCruiseNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;
        String expected = "error";

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userFromDB));
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(null);

        assertEquals(expected, underTest.paidForCruise(userId, cruiseId));
    }

    @Test
    void checkPaidForCruiseIfUserBalanceLowerThanCruisePrice() {
        Long userId = 1L;
        Long cruiseId = 1L;
        String expected = "lowBalance";

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userFromDB));
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(cruiseFromDB);

        userFromDB.setAccount(new BigDecimal(100));
        cruiseFromDB.setPrice(new BigDecimal(1000));

        assertEquals(expected, underTest.paidForCruise(userId, cruiseId));
    }

    @Test
    void checkPaidForCruiseIfBookingIsNull() {
        Long userId = 1L;
        Long cruiseId = 1L;
        String expected = "error";

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userFromDB));
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(cruiseFromDB);

        userFromDB.setAccount(new BigDecimal(1000));
        cruiseFromDB.setPrice(new BigDecimal(100));

        Mockito.when(userRepository.save(userFromDB)).thenReturn(userFromDB);
        Mockito.when(bookingService.findBookingById(userId, cruiseId)).thenReturn(null);

        assertEquals(expected, underTest.paidForCruise(userId, cruiseId));
    }

    @Test
    void checkIsPaidForCruiseReducesUserBalanceAndSetPaidUpForBooking() {
        Long userId = 1L;
        Long cruiseId = 1L;
        String expected = "success";
        Booking booking = new Booking();
        BigDecimal userBalanceAfterPaidUp = new BigDecimal(900);

        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userFromDB));
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(cruiseFromDB);

        userFromDB.setAccount(new BigDecimal(1000));
        cruiseFromDB.setPrice(new BigDecimal(100));

        Mockito.when(userRepository.save(userFromDB)).thenReturn(userFromDB);
        Mockito.when(bookingService.findBookingById(userId, cruiseId)).thenReturn(booking);
        Mockito.when(bookingRepository.save(booking)).thenReturn(booking);

        assertEquals(expected, underTest.paidForCruise(userId, cruiseId));
        assertTrue(booking.isPaidUp());
        assertEquals(userBalanceAfterPaidUp, userFromDB.getAccount());
    }

    @Test
    void findPaginated() {
        int pageNumber = 1;
        int pageSize = 1;

        Page page = mock(Page.class);

        Mockito.when(userRepository.findAll((Pageable) Mockito.any())).thenReturn(page);

        assertEquals(page.getTotalElements(), underTest.findPaginated(pageNumber, pageSize).getTotalElements());
    }
}