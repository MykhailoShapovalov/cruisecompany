package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.*;
import com.shapovalov.cruise_company.repository.BookingRepository;
import com.shapovalov.cruise_company.wrappers.UserCruiseBookingShipWrapper;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserCruiseBookingShipWrapperServiceTest {

    @Mock
    private BookingRepository bookingRepository;

    @Mock
    private UserService userService;

    @Mock
    private CruiseService cruiseService;

    private UserCruiseBookingShipWrapperService underTest;
    private User userFromDB;
    private Cruise cruiseFromDB;
    private Ship shipFromDB;
    private Booking bookingFromDB;

    @BeforeEach
    void setUp() {
        underTest = new UserCruiseBookingShipWrapperService(bookingRepository, userService, cruiseService);

        Set<Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));

        userFromDB = new User(1L, "agentSmith", "123456789a", "agentSmith@gmail.com", "John", "Smith", "0996573123",
                new BigDecimal(100), LocalDateTime.now(), LocalDateTime.now(), "123456789a", roles);

        Set<Ship> ships = new HashSet<>();
        Set<Cruise> cruises = new HashSet<>();

        shipFromDB = new Ship(1, "shipName", 900,
                0, 24, cruises);

        ships.add(shipFromDB);

        cruiseFromDB = new Cruise(1L, "cruiseName", "route",
                LocalDateTime.now(), LocalDateTime.now(), 5,
                BigDecimal.valueOf(1000), true, 6, ships);

        cruises.add(cruiseFromDB);

        bookingFromDB = new Booking(1L, 1L, false, false, false, true);
    }

    @Test
    void getWrappersShouldReturnEmptyListIfBookingsNotExist() {
        Mockito.when(bookingRepository.findAll()).thenReturn(new ArrayList<>());

        assertTrue(underTest.getWrappers().isEmpty());
    }

    @Test
    void getWrappersShouldReturnNullIfUserNotExist() {
        Long userId = 1L;
        List<Booking> bookings = new ArrayList<>();
        bookings.add(bookingFromDB);

        Mockito.when(bookingRepository.findAll()).thenReturn(bookings);
        Mockito.when(userService.findUserById(userId)).thenReturn(null);

        assertNull(underTest.getWrappers());
    }

    @Test
    void getWrappersShouldReturnNullIfCruiseNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;
        List<Booking> bookings = new ArrayList<>();
        bookings.add(bookingFromDB);

        Mockito.when(bookingRepository.findAll()).thenReturn(bookings);
        Mockito.when(userService.findUserById(userId)).thenReturn(userFromDB);
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(null);

        assertNull(underTest.getWrappers());
    }

    @Test
    void checkIfGetWrappersSetFieldsUserCruiseShipBooking() {
        Long userId = 1L;
        Long cruiseId = 1L;
        List<Booking> bookings = new ArrayList<>();
        bookings.add(bookingFromDB);

        Mockito.when(bookingRepository.findAll()).thenReturn(bookings);
        Mockito.when(userService.findUserById(userId)).thenReturn(userFromDB);
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(cruiseFromDB);

        assertFalse(underTest.getWrappers().isEmpty());
        Optional<UserCruiseBookingShipWrapper> first = underTest.getWrappers().stream().findFirst();

        assertEquals(bookingFromDB, first.get().getBooking());
        assertEquals(userFromDB, first.get().getUser());
        assertEquals(cruiseFromDB, first.get().getCruise());
        assertEquals(shipFromDB, first.get().getShips().stream().findFirst().get());
    }


    @Test
    void checkIfGetWrappersByUserIdSetFieldsUserCruiseShipsBooking() {
        Long userId = 1L;
        Long cruiseId = 1L;
        List<Booking> bookings = new ArrayList<>();
        bookings.add(bookingFromDB);

        Mockito.when(bookingRepository.findByUserId(userId)).thenReturn(bookings);
        Mockito.when(userService.findUserById(userId)).thenReturn(userFromDB);
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(cruiseFromDB);

        assertFalse(underTest.getWrappersByUserId(userId).isEmpty());
        Optional<UserCruiseBookingShipWrapper> first = underTest.getWrappersByUserId(userId).stream().findFirst();

        assertEquals(bookingFromDB, first.get().getBooking());
        assertEquals(userFromDB, first.get().getUser());
        assertEquals(cruiseFromDB, first.get().getCruise());
        assertEquals(shipFromDB, first.get().getShips().stream().findFirst().get());
    }

    @Test
    void getWrappersByUserIdShouldReturnNullIfUserNotExist() {
        Long userId = 1L;
        List<Booking> bookings = new ArrayList<>();
        bookings.add(bookingFromDB);

        Mockito.when(bookingRepository.findByUserId(userId)).thenReturn(bookings);
        Mockito.when(userService.findUserById(userId)).thenReturn(null);

        assertNull(underTest.getWrappersByUserId(userId));
    }

    @Test
    void getWrappersByUserIdShouldReturnNullIfCruiseNotExist() {
        Long userId = 1L;
        Long cruiseId = 1L;
        List<Booking> bookings = new ArrayList<>();
        bookings.add(bookingFromDB);

        Mockito.when(bookingRepository.findByUserId(userId)).thenReturn(bookings);
        Mockito.when(userService.findUserById(userId)).thenReturn(userFromDB);
        Mockito.when(cruiseService.findCruiseById(cruiseId)).thenReturn(null);

        assertNull(underTest.getWrappersByUserId(userId));
    }
}