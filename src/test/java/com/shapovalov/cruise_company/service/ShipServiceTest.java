package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.repository.ShipRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class ShipServiceTest {

    @Mock
    private ShipRepository shipRepository;

    private ShipService underTest;

    private Ship shipFromDB;

    @BeforeEach
    void setUp() {
        underTest = new ShipService(shipRepository);

        Set<Ship> ships = new HashSet<>();
        Set<Cruise> cruises = new HashSet<>();

        shipFromDB = new Ship(1, "shipName", 900,
                0, 24, cruises);

        ships.add(shipFromDB);

        Cruise cruiseFromDB = new Cruise(1L, "cruiseName", "route",
                LocalDateTime.now(), LocalDateTime.now(), 5,
                BigDecimal.valueOf(1000), true, 6, ships);

        cruises.add(cruiseFromDB);
    }

    @Test
    void getAllShips() {
        List<Ship> ships = new ArrayList<>();
        ships.add(shipFromDB);

        Mockito.when(shipRepository.findAll()).thenReturn(ships);

        assertEquals(shipFromDB.getShipName(), underTest.getAllShips().stream().findFirst().get().getShipName());
    }

    @Test
    void checkSaveNewShipIfShipFormNUll() {
        assertFalse(underTest.saveNewShip(null));
    }

    @Test
    void checkIfSaveNewShipSetsFields() {
        shipFromDB.setCurrentNumOfPeople(100);

        Mockito.when(shipRepository.save(shipFromDB)).thenReturn(shipFromDB);

        assertTrue(underTest.saveNewShip(shipFromDB));
        assertEquals(0, shipFromDB.getCurrentNumOfPeople());
    }

    @Test
    void findShipByIdShouldReturnNullIfShipNotExist() {
        int id = 1;

        Mockito.when(shipRepository.findById(any())).thenReturn(Optional.empty());

        assertNull(underTest.findShipById(id));
    }

    @Test
    void findShipByIdShouldReturnShip() {
        int id = 1;

        Mockito.when(shipRepository.findById(id)).thenReturn(Optional.of(shipFromDB));

        assertEquals(shipFromDB, underTest.findShipById(id));
    }

    @Test
    void checkIsUpdateShipSetsFields() {
        int id = 1;
        Ship shipBeforeUpdate = new Ship();

        Mockito.when(shipRepository.getById(id)).thenReturn(shipBeforeUpdate);
        Mockito.when(shipRepository.save(shipBeforeUpdate)).thenReturn(shipBeforeUpdate);

        underTest.updateShip(id, shipFromDB);

        assertEquals(shipFromDB.getShipName(), shipBeforeUpdate.getShipName());
        assertEquals(shipFromDB.getCapacity(), shipBeforeUpdate.getCapacity());
        assertEquals(shipFromDB.getStuff(), shipBeforeUpdate.getStuff());
        assertEquals(shipFromDB.getCurrentNumOfPeople(), shipBeforeUpdate.getCurrentNumOfPeople());
    }

    @Test
    void deleteShipShouldReturnFalseIfShipNotExist() {
        int id = 1;

        Mockito.doThrow(IllegalArgumentException.class).when(shipRepository).deleteById(any());

        assertFalse(underTest.deleteShip(id));
    }

    @Test
    void deleteShipShouldReturnTrueIfShipExist() {
        int id = 1;

        Mockito.doNothing().when(shipRepository).deleteById(any());

        assertTrue(underTest.deleteShip(id));
    }

    @Test
    void getFreeShipsShouldReturnEmptyListIfNoFreeShips() {
        List<Ship> ships = new ArrayList<>();
        ships.add(shipFromDB);

        Mockito.when(shipRepository.findAll()).thenReturn(ships);

        assertTrue(underTest.getFreeShips().isEmpty());
    }

    @Test
    void getFreeShipsShouldReturnListWithOneShipIfOnlyItFree() {
        List<Ship> ships = new ArrayList<>();
        shipFromDB.setShips_cruises(new HashSet<>());
        ships.add(shipFromDB);

        Mockito.when(shipRepository.findAll()).thenReturn(ships);

        assertFalse(underTest.getFreeShips().isEmpty());
    }
}