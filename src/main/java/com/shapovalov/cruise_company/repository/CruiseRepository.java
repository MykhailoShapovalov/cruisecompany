package com.shapovalov.cruise_company.repository;

import com.shapovalov.cruise_company.entity.Cruise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CruiseRepository extends JpaRepository<Cruise, Long> {
}