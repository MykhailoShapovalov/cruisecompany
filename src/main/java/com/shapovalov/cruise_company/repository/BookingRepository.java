package com.shapovalov.cruise_company.repository;

import com.shapovalov.cruise_company.entity.Booking;
import com.shapovalov.cruise_company.entity.BookingId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, BookingId> {
    List<Booking> findByUserId(Long userId);
    List<Booking> findByCruiseId(Long cruiseId);
}