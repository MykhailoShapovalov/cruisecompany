package com.shapovalov.cruise_company.repository;

import com.shapovalov.cruise_company.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}