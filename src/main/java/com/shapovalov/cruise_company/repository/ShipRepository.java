package com.shapovalov.cruise_company.repository;

import com.shapovalov.cruise_company.entity.Ship;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipRepository extends JpaRepository<Ship, Integer> {
}