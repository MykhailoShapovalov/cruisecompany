package com.shapovalov.cruise_company.repository;

import com.shapovalov.cruise_company.entity.Documents;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentsRepository extends JpaRepository<Documents, Long> {
}