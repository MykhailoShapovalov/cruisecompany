package com.shapovalov.cruise_company.wrappers;

import com.shapovalov.cruise_company.entity.Booking;
import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.entity.User;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

// класс використовується для створення складених об'єктів для спрощення виводу інформації на UI
@Getter
@Setter
public class UserCruiseBookingShipWrapper {

    private User user;
    private Cruise cruise;
    private Booking booking;
    private Set<Ship> ships;

    public UserCruiseBookingShipWrapper() {
    }

    public UserCruiseBookingShipWrapper(User user, Cruise cruise, Booking booking, Set<Ship> ships) {
        this.user = user;
        this.cruise = cruise;
        this.booking = booking;
        this.ships = ships;
    }
}