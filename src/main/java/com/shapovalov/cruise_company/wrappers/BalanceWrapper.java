package com.shapovalov.cruise_company.wrappers;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

// Клас використовується при поповненні балансу для валідації вводних данних.
@Getter
@Setter
public class BalanceWrapper {

    @NotEmpty(message = "This field should`t be empty.")
    @Pattern(regexp = "^\\d{1,5}(\\.\\d{2})?", message = "The amount must be valid. Use '.' as a delimiter, if you need.")
    private String balance;
}