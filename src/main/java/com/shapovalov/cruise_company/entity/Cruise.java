package com.shapovalov.cruise_company.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "t_cruise")
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Cruise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "The field should not be empty.")
    @Column(name = "cruise_name")
    private String cruiseName;

    @NotEmpty(message = "The field should not be empty.")
    @Column(name = "route")
    private String route;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime endDate;

    @Column(name = "ports_during_route")
    private int portsDuringRoute;

    @DecimalMin("0")
    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "free_places")
    private boolean freePlaces;

    @Column(name = "duration")
    private int duration;

    @ManyToMany(mappedBy = "ships_cruises")
    private Set<Ship> ships;

    public Long getId() {
        return id;
    }

    public String getCruiseName() {
        return cruiseName;
    }

    public String getRoute() {
        return route;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public int getPortsDuringRoute() {
        return portsDuringRoute;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public boolean isFreePlaces() {
        return freePlaces;
    }

    public int getDuration() {
        return duration;
    }

    public Set<Ship> getShips() {
        return ships;
    }

    @Override
    public String toString() {
        return "Cruise{" +
                "id=" + id +
                ", cruiseName='" + cruiseName + '\'' +
                ", route='" + route + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", portsDuringRoute=" + portsDuringRoute +
                ", price=" + price +
                ", freePlaces=" + freePlaces +
                ", duration=" + duration +
                ", ships=" + ships +
                '}';
    }
}