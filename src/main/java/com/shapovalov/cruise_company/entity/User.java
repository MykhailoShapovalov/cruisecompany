package com.shapovalov.cruise_company.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Set;

@Entity
@ToString
@Getter
@Setter
@AllArgsConstructor
@Table(name = "t_user")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 5, message = "At least 5 characters")
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Email(message = "Email should be valid.")
    @NotEmpty(message = "Email should not be empty.")
    @Column(name = "email")
    private String email;

    @NotEmpty(message = "First name should not be empty.")
    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 characters and consist only of letters.")
    @Pattern(regexp = "[a-zA-Z]+", message = "Name should be between 2 and 30 characters and consist only of letters.")
    @Column(name = "first_name")
    private String firstName;

    @NotEmpty(message = "Last name should not be empty.")
    @Pattern(regexp = "[a-zA-Z]+", message = "Last Name should be between 3 and 30 characters and consist only of letters.")
    @Size(min = 3, max = 30, message = "Last Name should be between 3 and 30 characters and consist only of letters.")
    @Column(name = "last_name")
    private String lastName;

    @NotEmpty(message = "Phone number should not be empty.")
    @Pattern(regexp = "(^$|[0-9]{10,15})", message = "Phone number should be valid")
    @Column(name = "phone")
    private String phone;

    @Column(name = "account")
    private BigDecimal account;

    @CreatedDate
    @Column(name = "created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "updated")
    private LocalDateTime updated;

    @Transient
    private String passwordConfirm;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "t_user_role",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private Set<Role> roles;

    public User() {
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public String getPassword() {
        return password;
    }
}