package com.shapovalov.cruise_company.entity;

import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import lombok.*;

@Entity
@Table(name = "t_ship")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Ship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "The field should not be empty.")
    @Column(name = "ship_name")
    private String shipName;

    @Column(name = "capacity")
    private int capacity;

    @Column(name = "current_num_of_people")
    private int currentNumOfPeople;

    @Column(name = "stuff")
    private int stuff;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "t_ship_cruise",
            joinColumns = {@JoinColumn(name = "ship_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "cruise_id", referencedColumnName = "id")})
    private Set<Cruise> ships_cruises;
}