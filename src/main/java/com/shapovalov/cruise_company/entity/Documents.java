package com.shapovalov.cruise_company.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "t_documents")
public class Documents {

    @Id
    private Long id;

    @Column(name = "foreign_passport")
    private String foreignPassport;

    public Documents() {
    }

    public Documents(Long id, String foreignPassport) {
        this.id = id;
        this.foreignPassport = foreignPassport;
    }
}