package com.shapovalov.cruise_company.entity;

import javax.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "t_booking")
@IdClass(BookingId.class)
@AllArgsConstructor
@NoArgsConstructor
public class Booking {

    @Id
    private Long userId;

    @Id
    private Long cruiseId;

    @Column(name = "approved")
    private boolean isApproved;

    @Column(name = "documents_uploaded")
    private boolean isDocumentUpload;

    @Column(name = "paid_up")
    private boolean paidUp;

    @Column(name = "active")
    private boolean isActive;
}