package com.shapovalov.cruise_company.entity;

import java.io.Serializable;
import java.util.Objects;

// клас представляє собою композитний ключ для БД
public class BookingId implements Serializable {

    private Long userId;
    private Long cruiseId;

    public BookingId() {
    }

    public BookingId(Long userId, Long cruiseId) {
        this.userId = userId;
        this.cruiseId = cruiseId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCruiseId() {
        return cruiseId;
    }

    public void setCruiseId(Long cruiseId) {
        this.cruiseId = cruiseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookingId bookingId = (BookingId) o;
        return Objects.equals(cruiseId, bookingId.cruiseId) && userId.equals(bookingId.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, cruiseId);
    }
}