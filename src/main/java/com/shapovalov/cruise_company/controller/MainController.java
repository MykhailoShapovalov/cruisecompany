package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.wrappers.BalanceWrapper;
import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.User;
import com.shapovalov.cruise_company.service.CruiseService;
import com.shapovalov.cruise_company.service.UserService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {

    private final UserService userService;
    private final CruiseService cruiseService;

    @Autowired
    public MainController(UserService userService, CruiseService cruiseService) {
        this.userService = userService;
        this.cruiseService = cruiseService;
    }

    @GetMapping("/")
    private String hello() {

        if (checkUserInstanceOfUserDetails().isPresent()) {
            return "redirect:/main";
        }

        return "welcome";
    }

    @GetMapping("/main")
    public String mainPage(Model model) {

        Optional<User> optionalUser = checkUserInstanceOfUserDetails();

        if (optionalUser.isEmpty()) {
            return "redirect:/";
        }

        model.addAttribute("user", optionalUser.get());

        return mainPagePaginationSorting(1, "cruiseName", "asc", model);
    }

    @GetMapping("/main/{pageNo}")
    public String mainPagePaginationSorting(@PathVariable(value = "pageNo") int pageNo,
                                            @RequestParam("sortField") String sortField,
                                            @RequestParam("sortDir") String sortDir,
                                            Model model) {

        Optional<User> optionalUser = checkUserInstanceOfUserDetails();

        if (optionalUser.isEmpty()) {
            return "redirect:/";
        }

        model.addAttribute("user", optionalUser.get());

        int pageSize = 5;
        Page<Cruise> page = cruiseService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<Cruise> cruiseList = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("cruiseList", cruiseList);

        return "main";
    }

    @GetMapping("/updateBalance")
    public String getUpdateBalanceForm(Model model) {

        Optional<User> optionalUser = checkUserInstanceOfUserDetails();

        if (optionalUser.isEmpty()) {
            return "redirect:/";
        }

        model.addAttribute("id", optionalUser.get().getId());
        model.addAttribute("balanceWrapper", new BalanceWrapper());

        return "updateBalance";
    }

    @PatchMapping("/updateBalance/{id}")
    public String updateBalance(@ModelAttribute("balanceWrapper") @Valid BalanceWrapper balanceWrapper,
                                BindingResult bindingResult,
                                @PathVariable(value = "id") Long id) {

        if (bindingResult.hasErrors()) {
            return "updateBalance";
        }

        if (!userService.updateBalance(id, new BigDecimal(balanceWrapper.getBalance()))) {
            return "redirect:/error";
        }

        return "redirect:/user/{id}";
    }

    private Optional<User> checkUserInstanceOfUserDetails() {
        Object currentUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!(currentUser instanceof UserDetails)) {
            return Optional.empty();
        }

        return Optional.of(userService.findUserById(((User) currentUser).getId()));
    }
}