package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.entity.User;
import com.shapovalov.cruise_company.service.SecurityService;
import com.shapovalov.cruise_company.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegistrationController {

    private final UserService userService;
    private final SecurityService securityService;

    @Autowired
    public RegistrationController(UserService userService, SecurityService securityService) {
        this.userService = userService;
        this.securityService = securityService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("userForm") @Valid User userForm,
                          BindingResult bindingResult,
                          HttpServletRequest request) {

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        if (!userForm.getPassword().matches("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")) {
            bindingResult.rejectValue("password", null,
                    "Password must contain minimum eight characters, at least one letter and one number!");
            return "registration";
        }

        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", null, "Password don`t match");
            return "registration";
        }

        if (userService.isEmailExist(userForm.getEmail())) {
            bindingResult.rejectValue("email", null, "User with this email already exists");
            return "registration";
        }

        if (userService.isUsernameExist(userForm.getUsername())) {
            bindingResult.rejectValue("username", null, "User with this username already exists");
            return "registration";
        }

        if (!userService.saveNewUser(userForm)) {
            return "redirect:/error";
        }

        if (!securityService.autoLogin(userForm.getUsername(), userForm.getPassword(), request)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }
}