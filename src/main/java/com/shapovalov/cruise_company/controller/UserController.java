package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.entity.User;
import com.shapovalov.cruise_company.service.BookingService;
import com.shapovalov.cruise_company.service.UserCruiseBookingShipWrapperService;
import com.shapovalov.cruise_company.service.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final UserCruiseBookingShipWrapperService userCruiseBookingShipWrapperService;
    private final BookingService bookingService;

    @Autowired
    public UserController(UserService userService, UserCruiseBookingShipWrapperService userCruiseBookingShipWrapperService, BookingService bookingService) {
        this.userService = userService;
        this.userCruiseBookingShipWrapperService = userCruiseBookingShipWrapperService;
        this.bookingService = bookingService;
    }

    @GetMapping("/{id}")
    public String showUser(@PathVariable("id") Long id, Model model) {
        User user = userService.findUserById(id);

        if (user == null) {
            return "redirect:/error";
        }

        model.addAttribute("user", user);

        return "showUser";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") Long id) {
        User user = userService.findUserById(id);

        if (user == null) {
            return "redirect:/error";
        }

        model.addAttribute("user", user);

        return "editUser";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("user") @Valid User user, BindingResult bindingResult,
                         @PathVariable("id") Long id) {

        if (bindingResult.hasErrors()) {
            return "editUser";
        }

        userService.updateUser(id, user);

        return "redirect:/user/{id}";
    }

    @DeleteMapping("/{id}/")
    public String delete(@PathVariable("id") Long id) {

        if (!userService.deleteUser(id)) {
            return "redirect:/error";
        }

        return "redirect:/logout";
    }

    @GetMapping("/myBooking/{userId}")
    public String showMyBooking(@PathVariable("userId") Long userId, Model model) {
        User user = userService.findUserById(userId);

        if (user == null) {
            return "redirect:/error";
        }

        model.addAttribute("wrappers", userCruiseBookingShipWrapperService.getWrappersByUserId(userId));
        model.addAttribute("user", user);
        return "showUserBooking";
    }

    @GetMapping("/myBooking/pay")
    public String showPayForm(@RequestParam("userId") Long userId,
                              @RequestParam("cruiseId") Long cruiseId,
                              Model model) {

        model.addAttribute("booking", bookingService.findBookingById(userId, cruiseId));

        return "payForCruise";
    }

    @PatchMapping("/myBooking/pay")
    public String payForCruise(@RequestParam("userId") Long userId,
                               @RequestParam("cruiseId") Long cruiseId) {

        String result = userService.paidForCruise(userId, cruiseId);

        if (result.equalsIgnoreCase("lowBalance")) {
            return "errors/lowBalance";
        } else if (result.equalsIgnoreCase("success")) {
            return "redirect:/main";
        } else {
            return "redirect:/error";
        }
    }
}