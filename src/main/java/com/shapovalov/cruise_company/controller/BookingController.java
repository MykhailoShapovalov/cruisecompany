package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.entity.User;
import com.shapovalov.cruise_company.service.BookingService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BookingController {

    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping("/booking")
    public String applyForTrip(@RequestParam("cruiseId") Long cruiseId) {

        Object currentUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!Objects.equals(bookingService.applyToCruise(((User) currentUser).getId(), cruiseId).getCruiseId(),
                cruiseId)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }
}