package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.service.ShipService;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class ShipController {

    private final ShipService shipService;

    public ShipController(ShipService shipService) {
        this.shipService = shipService;
    }

    @GetMapping("/main/ships")
    public String showShipsForUser(Model model) {
        model.addAttribute("ships", shipService.getAllShips());
        return "showShipsForUser";
    }

    @GetMapping("/admin/ships")
    public String showShipsForAdmin(Model model) {
        model.addAttribute("ships", shipService.getAllShips());
        return "showShipsForAdmin";
    }

    @GetMapping("/admin/ship")
    public String getShipForm(Model model) {
        model.addAttribute("shipForm", new Ship());
        return "addShip";
    }

    @PostMapping("/admin/ship")
    public String addShip(@ModelAttribute("shipForm") @Valid Ship shipForm,
                          BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "addShip";
        }

        if (!shipService.saveNewShip(shipForm)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }

    @GetMapping("/admin/ship/{id}")
    public String editShipForm(@PathVariable(value = "id") int shipId,
                               Model model) {

        Ship ship = shipService.findShipById(shipId);

        if (ship == null) {
            return "redirect:/error";
        }

        model.addAttribute("ship", ship);

        return "editShip";
    }


    @PatchMapping("/admin/ship/{id}")
    public String editShip(@ModelAttribute("ship") @Valid Ship ship,
                           BindingResult bindingResult,
                           @PathVariable(value = "id") int shipId) {

        if (bindingResult.hasErrors()) {
            return "editShip";
        }

        shipService.updateShip(shipId, ship);

        return "redirect:/main";
    }

    @DeleteMapping("/admin/ship/{id}")
    public String deleteShip(@PathVariable("id") int shipId) {

        if (!shipService.deleteShip(shipId)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }
}