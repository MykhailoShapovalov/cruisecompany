package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.service.BookingService;
import com.shapovalov.cruise_company.service.DocumentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class DocumentsController {

    private final DocumentsService documentsService;
    private final BookingService bookingService;

    @Autowired
    public DocumentsController(DocumentsService documentsService, BookingService bookingService) {
        this.documentsService = documentsService;
        this.bookingService = bookingService;
    }

    @GetMapping("/user/myBooking/uploadDocuments")
    public String getUploadForm(@RequestParam("userId") Long userId,
                                @RequestParam("cruiseId") Long cruiseId,
                                Model model) {

        model.addAttribute("booking", bookingService.findBookingById(userId, cruiseId));

        return "uploadDocuments";
    }

    @PostMapping("/user/myBooking/uploadDocuments")
    public String uploadDocuments(@RequestParam("foreignPassport") MultipartFile foreignPassport,
                                  @RequestParam("userId") Long userId,
                                  @RequestParam("cruiseId") Long cruiseId) {

        if (!documentsService.saveImage(foreignPassport, userId)) {
            return "redirect:/error";
        }

        if (!bookingService.updateIsDocumentUpload(userId, cruiseId)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }
}