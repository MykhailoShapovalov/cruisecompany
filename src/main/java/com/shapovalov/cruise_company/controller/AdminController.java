package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.entity.User;
import com.shapovalov.cruise_company.service.BookingService;
import com.shapovalov.cruise_company.service.UserCruiseBookingShipWrapperService;
import com.shapovalov.cruise_company.service.UserService;
import com.shapovalov.cruise_company.wrappers.UserCruiseBookingShipWrapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

    private final UserService userService;
    private final UserCruiseBookingShipWrapperService userCruiseBookingShipWrapperService;
    private final BookingService bookingService;

    @Autowired
    public AdminController(UserService userService, UserCruiseBookingShipWrapperService userCruiseBookingShipWrapperService, BookingService bookingService) {
        this.userService = userService;
        this.userCruiseBookingShipWrapperService = userCruiseBookingShipWrapperService;
        this.bookingService = bookingService;
    }

    @GetMapping("/admin/{id}")
    public String showAdmin(@PathVariable("id") Long id, Model model) {

        User user = userService.findUserById(id);

        if (user == null) {
            return "redirect:/error";
        }

        model.addAttribute("user", user);
        return "showAdmin";
    }

    @GetMapping("/admin/getUsers")
    public String getUserList(Model model) {
        return userList(1, model);
    }

    @GetMapping("/admin/getUsers/{pageNo}")
    public String userList(@PathVariable(value = "pageNo") int pageNo, Model model) {
        int pageSize = 5;

        Page<User> page = userService.findPaginated(pageNo, pageSize);
        List<User> userList = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("userList", userList);

        return "allUsers";
    }

    @GetMapping("/admin/user/{id}/booking")
    public String bookingInfo(@PathVariable("id") Long userId, Model model) {
        User user = userService.findUserById(userId);

        if (user == null) {
            return "redirect:/error";
        }

        model.addAttribute("user", user);
        model.addAttribute("wrappers", userCruiseBookingShipWrapperService.getWrappersByUserId(userId));
        return "showUserBookingForAdmin";
    }

    @GetMapping("/admin/usersCruises")
    public String getUsersCruises(Model model) {
        return usersCruises(1, model);
    }

    @GetMapping("/admin/usersCruises/{pageNo}")
    public String usersCruises(@PathVariable(value = "pageNo") int pageNo, Model model) {
        int pageSize = 4;

        Page<UserCruiseBookingShipWrapper> page = userCruiseBookingShipWrapperService.getPageWrappers(pageNo, pageSize);
        List<UserCruiseBookingShipWrapper> wrappers = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("wrappers", wrappers);

        return "usersCruises";
    }

    @PostMapping("/admin/usersCruises")
    public String confirmBooking(@RequestParam("userId") Long userId,
                                 @RequestParam("cruiseId") Long cruiseId) {

        if (!bookingService.confirmApplyToCruise(userId, cruiseId)) {
            return "redirect:/error";
        }

        return "redirect:/admin/usersCruises";
    }
}