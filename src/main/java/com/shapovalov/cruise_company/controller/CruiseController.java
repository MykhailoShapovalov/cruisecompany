package com.shapovalov.cruise_company.controller;

import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.User;
import com.shapovalov.cruise_company.service.BookingService;
import com.shapovalov.cruise_company.service.CruiseService;
import com.shapovalov.cruise_company.service.ShipService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping
public class CruiseController {

    private final CruiseService cruiseService;
    private final BookingService bookingService;
    private final ShipService shipService;

    @Autowired
    public CruiseController(CruiseService cruiseService, BookingService bookingService, ShipService shipService) {
        this.cruiseService = cruiseService;
        this.bookingService = bookingService;
        this.shipService = shipService;
    }

    @GetMapping("/cruise/{id}")
    public String showCruise(@PathVariable(value = "id") Long cruiseId,
                             Model model) {
        // При кожному переході на сторінку, веревіряє наявність вільних місць та сетить значення
        cruiseService.checkFreePlaces(cruiseId);

        Long userId = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();

        Cruise cruise = cruiseService.findCruiseById(cruiseId);

        if (cruise == null) {
            return "redirect:/error";
        }

        model.addAttribute("isBookingExist", bookingService.isBookingExist(userId, cruiseId));
        model.addAttribute("cruise", cruise);

        return "cruiseInfo";
    }

    @GetMapping("/admin/cruises")
    public String getAllCruises(Model model) {
        return showAllCruises(1, model);
    }

    @GetMapping("/admin/cruises/{pageNo}")
    public String showAllCruises(@PathVariable(value = "pageNo") int pageNo, Model model) {
        int pageSize = 3;

        Page<Cruise> page = cruiseService.findPaginated(pageNo, pageSize);
        List<Cruise> cruises = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("cruises", cruises);

        return "showAllCruisesForAdmin";
    }

    @GetMapping("/admin/cruise")
    public String getCruiseForm(Model model) {
        model.addAttribute("cruiseForm", new Cruise());
        model.addAttribute("ships", shipService.getAllShips());
        return "createCruise";
    }

    @PostMapping("/admin/cruise")
    public String createCruise(@ModelAttribute("cruiseForm") @Valid Cruise cruiseForm,
                               BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "createCruise";
        }

        if (cruiseForm.getStartDate().isAfter(cruiseForm.getEndDate())) {
            bindingResult.rejectValue("startDate", null, "Start Date should not be after End Date");
            return "createCruise";
        }

        if (!cruiseService.saveNewCruise(cruiseForm)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }

    @GetMapping("/admin/cruise/{id}/ship")
    public String addShipToCruiseForm(@PathVariable(value = "id") Long cruiseId,
                                      Model model) {

        Cruise cruise = cruiseService.findCruiseById(cruiseId);

        if (cruise == null) {
            return "redirect:/error";
        }

        model.addAttribute("cruise", cruise);
        model.addAttribute("ships", shipService.getFreeShips());

        return "addShipToCruise";
    }

    @PostMapping("/admin/cruise/{id}/ship/{shipId}")
    public String addShipToCruise(@PathVariable(value = "id") Long cruiseId,
                                  @PathVariable(value = "shipId") int shipId) {

        if (!cruiseService.addShipToCruise(cruiseId, shipId)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }

    @GetMapping("/admin/cruise/{id}")
    public String getUpdateForm(@PathVariable(value = "id") Long cruiseId,
                                Model model) {

        Cruise cruise = cruiseService.findCruiseById(cruiseId);

        if (cruise == null) {
            return "redirect:/error";
        }

        model.addAttribute("cruise", cruise);

        return "editCruise";
    }

    @PatchMapping("/admin/cruise/{id}")
    public String updateCruise(@ModelAttribute("cruise") @Valid Cruise cruise,
                               BindingResult bindingResult,
                               @PathVariable(value = "id") Long cruiseId) {

        if (bindingResult.hasErrors()) {
            return "editCruise";
        }

        cruiseService.updateCruise(cruise, cruiseId);

        return "redirect:/main";
    }

    @DeleteMapping("/admin/cruise/{id}")
    public String deleteCruise(@PathVariable("id") Long cruiseId) {

        if (!cruiseService.deleteById(cruiseId)) {
            return "redirect:/error";
        }

        return "redirect:/main";
    }
}