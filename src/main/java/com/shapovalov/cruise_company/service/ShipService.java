package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.repository.ShipRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ShipService {

    private final ShipRepository shipRepository;

    @Autowired
    public ShipService(ShipRepository shipRepository) {
        this.shipRepository = shipRepository;
    }

    public List<Ship> getAllShips() {
        return shipRepository.findAll();
    }

    public boolean saveNewShip(Ship shipForm) {
        if (shipForm == null) {
            log.error("Error during saving new ship. Method: saveNewShip(). ShipForm is null!");
            return false;
        }
        shipForm.setCurrentNumOfPeople(0);

        shipRepository.save(shipForm);
        return true;
    }

    public Ship findShipById(int id) {
        Ship ship;

        try {
            ship = shipRepository.findById(id).orElseThrow(
                    () -> new IllegalArgumentException("Ships with id: " + id + " not found! Method findShipById()")
            );
        } catch (IllegalArgumentException e) {
            log.error("Error during finding ship by id. Method: findShipById()", e);
            return null;
        }

        return ship;
    }

    public void updateShip(int shipId, Ship ship) {
        Ship shipFromDB = shipRepository.getById(shipId);

        shipFromDB.setShipName(ship.getShipName());
        shipFromDB.setCapacity(ship.getCapacity());
        shipFromDB.setStuff(ship.getStuff());
        shipFromDB.setCurrentNumOfPeople(ship.getCurrentNumOfPeople());

        shipRepository.save(shipFromDB);
    }

    public boolean deleteShip(int shipId) {
        try {
            shipRepository.deleteById(shipId);
        } catch (IllegalArgumentException e) {
            log.error("Error during deleting ship. Method: deleteShip()", e);
            return false;
        }

        return true;
    }

    // повертає список кораблів які не прив'язані до круїзу
    public List<Ship> getFreeShips() {
        List<Ship> ships = new ArrayList<>();
        List<Ship> allShips = shipRepository.findAll();

        for (Ship ship : allShips) {
            if (ship.getShips_cruises().isEmpty()) {
                ships.add(ship);
            }
        }

        return ships;
    }
}