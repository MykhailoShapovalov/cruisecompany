package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.repository.CruiseRepository;
import com.shapovalov.cruise_company.repository.ShipRepository;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CruiseService {

    private final CruiseRepository cruiseRepository;
    private final ShipRepository shipRepository;
    private final ShipService shipService;

    @Autowired
    public CruiseService(CruiseRepository cruiseRepository, ShipRepository shipRepository, ShipService shipService) {
        this.cruiseRepository = cruiseRepository;
        this.shipRepository = shipRepository;
        this.shipService = shipService;
    }

    public Cruise findCruiseById(Long id) {
        Optional<Cruise> cruiseFromDB = cruiseRepository.findById(id);

        if (cruiseFromDB.isEmpty()) {
            log.error("Error during finding cruise. Method: getCruiseById()");
            return null;
        }

        return cruiseFromDB.get();
    }

    public List<Cruise> getAll() {
        return cruiseRepository.findAll();
    }

    public Page<Cruise> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return this.cruiseRepository.findAll(pageable);
    }

    // перевіряє наявність вільних місць на всіх кораблях круїзу та сетить відповідне значення
    public void checkFreePlaces(Long cruiseId) {
        Cruise cruise = findCruiseById(cruiseId);

        if (cruise == null) {
            return;
        }

        Set<Ship> ships = cruise.getShips();

        for (Ship ship : ships) {
            if (ship.getCapacity() - ship.getCurrentNumOfPeople() > 0) {
                cruise.setFreePlaces(true);
                cruiseRepository.save(cruise);
                return;
            }
        }

        cruise.setFreePlaces(false);
        cruiseRepository.save(cruise);
    }

    // зберігає новий круїз та сетить значення freePlaces - true
    public boolean saveNewCruise(Cruise cruiseForm) {
        if (cruiseForm == null) {
            log.error("Error during saving new cruise. CruiseForm is null! Method: saveNewCruise()");
            return false;
        }

        cruiseForm.setFreePlaces(true);
        cruiseRepository.save(cruiseForm);
        return true;
    }

    public boolean deleteById(Long cruiseId) {

        try {
            cruiseRepository.deleteById(cruiseId);
        } catch (IllegalArgumentException e) {
            log.error("Error during deleting cruise. Method: deleteById(). Cruise id: " + cruiseId, e);
            return false;
        }

        return true;
    }

    // додає корабель до круїзу
    public boolean addShipToCruise(Long cruiseId, int shipId) {
        Cruise cruise = findCruiseById(cruiseId);
        Ship ship = shipService.findShipById(shipId);

        if (cruise == null || ship == null) {
            return false;
        }

        ship.getShips_cruises().add(cruise);
        cruise.getShips().add(ship);

        cruiseRepository.save(cruise);
        shipRepository.save(ship);

        return true;
    }

    public void updateCruise(Cruise cruise, Long cruiseId) {
        Cruise cruiseFromDB = cruiseRepository.getById(cruiseId);

        cruiseFromDB.setCruiseName(cruise.getCruiseName());
        cruiseFromDB.setRoute(cruise.getRoute());
        cruiseFromDB.setStartDate(cruise.getStartDate());
        cruiseFromDB.setEndDate(cruise.getEndDate());
        cruiseFromDB.setPortsDuringRoute(cruise.getPortsDuringRoute());
        cruiseFromDB.setPrice(cruise.getPrice());
        cruiseFromDB.setDuration(cruise.getDuration());
        cruiseFromDB.setFreePlaces(cruise.isFreePlaces());

        cruiseRepository.save(cruiseFromDB);
    }

    public Page<Cruise> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return this.cruiseRepository.findAll(pageable);
    }
}