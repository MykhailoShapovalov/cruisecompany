package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Documents;
import com.shapovalov.cruise_company.repository.DocumentsRepository;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Log4j2
@Service
public class DocumentsService {

    private final DocumentsRepository documentsRepository;

    @Autowired
    public DocumentsService(DocumentsRepository documentsRepository) {
        this.documentsRepository = documentsRepository;
    }

    public boolean saveImage(MultipartFile foreignPassport, Long userId) {
        String folder = "/Users/misha/Projects/IdeaProjects/cruise_company/src/main/resources/userDocuments/";
        byte[] bytes;

        try {
            bytes = foreignPassport.getBytes();
        } catch (IOException e) {
            log.error("Error during parsing document to bytes. Method: saveImage()", e);
            return false;
        }

        try {
            documentsRepository.save(new Documents(userId, Base64.getEncoder().encodeToString(bytes)));
        } catch (IllegalArgumentException e) {
            log.error("Error during saving new document. Method: saveImage()", e);
            return false;
        }

        Path path = Paths.get(folder + foreignPassport.getOriginalFilename());

        try {
            Files.write(path, bytes);
        } catch (IOException e) {
            log.error("Error during writing bytes to file. Method: saveImage()", e);
            return false;
        }

        return true;
    }
}