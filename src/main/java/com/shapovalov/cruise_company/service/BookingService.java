package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Booking;
import com.shapovalov.cruise_company.entity.BookingId;
import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.Ship;
import com.shapovalov.cruise_company.repository.BookingRepository;
import com.shapovalov.cruise_company.repository.CruiseRepository;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@EnableScheduling
public class BookingService {

    private final BookingRepository bookingRepository;
    private final CruiseRepository cruiseRepository;

    @Autowired
    public BookingService(BookingRepository bookingRepository, CruiseRepository cruiseRepository) {
        this.bookingRepository = bookingRepository;
        this.cruiseRepository = cruiseRepository;
    }

    // (юзер) подає заявку на круїз(створюється нова сутність booking)
    public Booking applyToCruise(Long userId, Long cruiseId) {
        Booking booking = new Booking();

        booking.setCruiseId(cruiseId);
        booking.setUserId(userId);
        booking.setApproved(false);
        booking.setDocumentUpload(false);
        booking.setPaidUp(false);
        booking.setActive(true);

        bookingRepository.save(booking);
        return booking;
    }

    // (адмін) підтверджує бронювання, збільшує кількість пасажирів на борту на 1
    public boolean confirmApplyToCruise(Long userId, Long cruiseId) {

        Set<Ship> shipList;

        try {
            shipList = cruiseRepository.findById(cruiseId).orElseThrow(
                    () -> new IllegalArgumentException("Cruise with id: " + cruiseId + " does not exist! Method: applyToCruise()")
            ).getShips();
        } catch (IllegalArgumentException e) {
            log.error("Error during finding shipList by cruiseId. Method: confirmApplyToCruise()", e);
            return false;
        }

        for (Ship ship : shipList) {
            if (ship.getCurrentNumOfPeople() == ship.getCapacity()) {
                log.info("Ship " + ship.getShipName() + " is full! Method: applyToCruise()");
                return false;
            }
        }

        shipList.forEach(ship -> ship.setCurrentNumOfPeople(ship.getCurrentNumOfPeople() + 1));

        Booking booking = findBookingById(userId, cruiseId);

        if (booking == null) {
            return false;
        }

        if (!booking.isApproved()) {
            booking.setApproved(true);
            bookingRepository.save(booking);
        } else {
            log.error("User already approved! Method: confirmApplyToCruise()");
            return false;
        }

        return true;
    }

    public Booking findBookingById(Long userId, Long cruiseId) {
        Optional<Booking> booking = bookingRepository.findById(new BookingId(userId, cruiseId));

        try {
            booking.orElseThrow(
                    () -> new IllegalArgumentException(
                            "Booking with userId, cruiseId: " + userId + ", " + cruiseId + " not found. Method: findById()")
            );
        } catch (IllegalArgumentException e) {
            log.error("Error during finding booking. Method: findById()", e);
            return null;
        }

        return booking.get();
    }

    // сетить поле IsDocumentUpload бронюванню - true
    public boolean updateIsDocumentUpload(Long userId, Long cruiseId) {
        Booking booking = findBookingById(userId, cruiseId);

        if (booking == null) {
            return false;
        }

        booking.setDocumentUpload(true);
        bookingRepository.save(booking);
        return true;
    }

    // перевіряє активність бронювань та встановлює false якщо воно прострочене
    @Scheduled(fixedRate = 3600000) // 10 000(10 секунд)
    public void checkIsBookingActive() {
        List<Cruise> cruiseList = cruiseRepository.findAll();

        for (Cruise cruise : cruiseList) {
            if (cruise.getStartDate().isBefore(LocalDateTime.now())) {
                List<Booking> bookings = bookingRepository.findByCruiseId(cruise.getId());
                bookings.forEach(it -> it.setActive(false));
                bookings.forEach(bookingRepository::save);
            }
        }

        System.out.println("The time is now " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
    }

    public Boolean isBookingExist(Long userId, Long cruiseId) {
        return bookingRepository.findById(new BookingId(userId, cruiseId)).isPresent();
    }
}