package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.*;
import com.shapovalov.cruise_company.repository.BookingRepository;
import com.shapovalov.cruise_company.repository.RoleRepository;
import com.shapovalov.cruise_company.repository.UserRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BookingRepository bookingRepository;
    private final CruiseService cruiseService;
    private final BookingService bookingService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, BookingRepository bookingRepository, CruiseService cruiseService, BookingService bookingService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bookingRepository = bookingRepository;
        this.cruiseService = cruiseService;
        this.bookingService = bookingService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user;

        try {
            user = userRepository.findByUsername(username).orElseThrow(
                    () -> new UsernameNotFoundException("User with username: " + username + " not found!")
            );
        } catch (UsernameNotFoundException e) {
            log.error("Error during finding user by username. Method: loadUserByUsername(). Username: " + username, e);
            return null;
        }

        return user;
    }

    public User findUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);

        if (userFromDb.isEmpty()) {
            log.error("Error during finding user by id. Method: findUserById(). Id: " + userId);
            return null;
        }

        return userFromDb.get();
    }

    public boolean isEmailExist(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    public boolean isUsernameExist(String username) {
        return userRepository.findByUsername(username).isPresent();
    }


    public boolean saveNewUser(User user) {
        if (user == null) {
            log.error("Error during saving new user. User is null. Method: saveNewUser()");
            return false;
        }

        setUserRole(user);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setAccount(new BigDecimal(0));
        user.setCreated(LocalDateTime.now());
        user.setUpdated(LocalDateTime.now());
        userRepository.save(user);

        return true;
    }

    // додатковий приватний метод, встановлює новому користувачу роль юзера
    public void setUserRole(User user) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.getById(1L));
        user.setRoles(roles);
    }

    // додатковий приватний метод, встановлює новому користувачу роль адміна
    public void setAdminRole(User user) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.getById(2L));
        user.setRoles(roles);
    }

    public void updateUser(Long id, User updatedUser) {
        User userFromDB = userRepository.getById(id);

        userFromDB.setFirstName(updatedUser.getFirstName());
        userFromDB.setLastName(updatedUser.getLastName());
        userFromDB.setEmail(updatedUser.getEmail());
        userFromDB.setPhone(updatedUser.getPhone());
        userFromDB.setUpdated(LocalDateTime.now());
        userRepository.save(userFromDB);
    }

    public boolean deleteUser(Long userId) {

        try {
            userRepository.deleteById(userId);
        } catch (IllegalArgumentException e) {
            log.error("Error during deleting user. Method: deleteUser(). User id: " + userId, e);
            return false;
        }

        return true;
    }

    @Transactional
    public boolean updateBalance(Long id, BigDecimal updateBalance) {
        User userFromDB = findUserById(id);

        if (userFromDB == null) {
            return false;
        }

        userFromDB.setAccount(userFromDB.getAccount().add(updateBalance));
        userRepository.save(userFromDB);

        return true;
    }

    @Transactional
    public String paidForCruise(Long userId, Long cruiseId) {
        User user = findUserById(userId);
        Cruise cruise = cruiseService.findCruiseById(cruiseId);

        if (user == null || cruise == null) {
            return "error";
        }

        if (cruise.getPrice().compareTo(user.getAccount()) > 0) {
            return "lowBalance";
        }

        user.setAccount(user.getAccount().subtract(cruise.getPrice()));
        userRepository.save(user);

        Booking booking = bookingService.findBookingById(userId, cruiseId);

        if (booking == null) {
            return "error";
        }

        booking.setPaidUp(true);
        bookingRepository.save(booking);

        return "success";
    }

    public Page<User> findPaginated(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize);
        return this.userRepository.findAll(pageable);
    }
}