package com.shapovalov.cruise_company.service;

import com.shapovalov.cruise_company.entity.Booking;
import com.shapovalov.cruise_company.entity.Cruise;
import com.shapovalov.cruise_company.entity.User;
import com.shapovalov.cruise_company.wrappers.UserCruiseBookingShipWrapper;
import com.shapovalov.cruise_company.repository.BookingRepository;
import java.util.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class UserCruiseBookingShipWrapperService {

    private final BookingRepository bookingRepository;
    private final UserService userService;
    private final CruiseService cruiseService;

    @Autowired
    public UserCruiseBookingShipWrapperService(BookingRepository bookingRepository, UserService userService, CruiseService cruiseService) {
        this.bookingRepository = bookingRepository;
        this.userService = userService;
        this.cruiseService = cruiseService;
    }

    // повертає складений об'єкт(для використання на UI) (всі бронювання)
    public List<UserCruiseBookingShipWrapper> getWrappers() {
        List<UserCruiseBookingShipWrapper> wrappers = new ArrayList<>();
        List<Booking> bookings = bookingRepository.findAll();
        for (Booking booking : bookings) {
            UserCruiseBookingShipWrapper wrapper = new UserCruiseBookingShipWrapper();

            User user = userService.findUserById(booking.getUserId());

            if (user == null) {
                log.error("Error during finding user. Method: getWrappers()");
                return null;
            }

            wrapper.setUser(user);

            Cruise cruise = cruiseService.findCruiseById(booking.getCruiseId());

            if (cruise == null) {
                log.error("Error during finding cruise. Method: getWrappers()");
                return null;
            }

            wrapper.setCruise(cruise);
            wrapper.setShips(cruise.getShips());
            wrapper.setBooking(booking);

            wrappers.add(wrapper);
        }

        return wrappers;
    }

    // повертає складений об'єкт(для використання на UI) (всі бронювання одного юзера)
    public List<UserCruiseBookingShipWrapper> getWrappersByUserId(Long userId) {
        List<UserCruiseBookingShipWrapper> wrappers = new ArrayList<>();
        List<Booking> bookingList = bookingRepository.findByUserId(userId);
        for (Booking booking : bookingList) {
            UserCruiseBookingShipWrapper wrapper = new UserCruiseBookingShipWrapper();

            User user = userService.findUserById(booking.getUserId());

            if (user == null) {
                log.error("Error during finding user. Method: getWrappers()");
                return null;
            }

            wrapper.setUser(user);

            Cruise cruise = cruiseService.findCruiseById(booking.getCruiseId());

            if (cruise == null) {
                log.error("Error during finding cruise. Method: getWrappers()");
                return null;
            }

            wrapper.setCruise(cruise);
            wrapper.setShips(cruise.getShips());
            wrapper.setBooking(booking);

            wrappers.add(wrapper);
        }
        return wrappers;
    }

    // повертає складений об'єкт(для використання на UI) для пагінації
    public Page<UserCruiseBookingShipWrapper> getPageWrappers(int pageNumber, int size) {
        PageRequest request = PageRequest.of(pageNumber - 1, size, Sort.by(Sort.Direction.ASC, "isApproved"));
        List<UserCruiseBookingShipWrapper> wrappers = new ArrayList<>();
        Page<Booking> bookings = bookingRepository.findAll(request);
        for (Booking booking : bookings) {
            UserCruiseBookingShipWrapper wrapper = new UserCruiseBookingShipWrapper();

            User user = userService.findUserById(booking.getUserId());

            if (user == null) {
                log.error("Error during finding user. Method: getWrappers()");
                return null;
            }

            wrapper.setUser(user);

            Cruise cruise = cruiseService.findCruiseById(booking.getCruiseId());

            if (cruise == null) {
                log.error("Error during finding cruise. Method: getWrappers()");
                return null;
            }

            wrapper.setCruise(cruise);
            wrapper.setShips(cruise.getShips());
            wrapper.setBooking(booking);

            wrappers.add(wrapper);
        }

        return new PageImpl<>(wrappers, request, bookingRepository.findAll().size());
    }
}