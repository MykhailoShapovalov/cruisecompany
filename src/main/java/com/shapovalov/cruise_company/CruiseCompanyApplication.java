package com.shapovalov.cruise_company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CruiseCompanyApplication {

    public static void main(String[] args) {
        SpringApplication.run(CruiseCompanyApplication.class, args);
    }
}